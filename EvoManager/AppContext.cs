﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

using EvoManager.Modules;

namespace EvoManager
{
    public class AppContext : ApplicationContext
    {
        private string IconFileName;
        private string DefaultTooltip = "EvoManager";

        public List<ToolStripItem> TrayMenuStripCollection;

        public AppContext()
        {
            IconFileName = (ConfigurationManager.AppSettings["TrayIcon"] != null) ? ConfigurationManager.AppSettings["TrayIcon"] : "Power.ico";
            TrayMenuStripCollection = new List<ToolStripItem>();

            InitializeContext();
            LoadModules();
        }

        #region generic code framework

        private System.ComponentModel.IContainer components;	// a list of components to dispose when the context is disposed
        private NotifyIcon SysTrayIcon;				            // the icon that sits in the system tray
        private ModuleManager ModMan;

        private void InitializeContext()
        {
            components = new System.ComponentModel.Container();
            SysTrayIcon = new NotifyIcon(components)
            {
                ContextMenuStrip = new ContextMenuStrip(),
                Icon = new Icon(IconFileName),
                Text = DefaultTooltip,
                Visible = true
            };
            SysTrayIcon.ContextMenuStrip.Opening += ContextMenuStrip_Opening;
            SysTrayIcon.MouseUp += SysTrayIcon_MouseUp;
        }

        private void LoadModules()
        {
            string ModuleFolder = (ConfigurationManager.AppSettings["ModuleFolder"] != null) ? ConfigurationManager.AppSettings["ModuleFolder"] : "Modules";
            ModMan = new ModuleManager(ModuleFolder);

            foreach (Modules.ModAssembly mod in ModMan.ModLibrary)
            {
                TrayMenuStripCollection.Add(ToolStripMenuItemWithHandler(mod.ModuleName, ModMan.ModuleOpen_ToolStripMenuItemClickEvent));
            }
        }

        /// <summary>
        /// When the application context is disposed, dispose things like the notify icon.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null) {
                components.Dispose();
                ModMan.Dispose();
            }
        }

        /// <summary>
        /// When the exit menu item is clicked, make a call to terminate the ApplicationContext.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitItem_Click(object sender, EventArgs e)
        {
            ExitThread();
        }

        /// <summary>
        /// If we are presently showing a form, clean it up.
        /// </summary>
        protected override void ExitThreadCore()
        {
            SysTrayIcon.Visible = false; // should remove lingering tray icon
            base.ExitThreadCore();
        }

        public ToolStripMenuItem ToolStripMenuItemWithHandler(string displayText, EventHandler eventHandler, string name = null)
        {
            var item = new ToolStripMenuItem(displayText);
            item.Name = (name != null) ? name : displayText;
            if (eventHandler != null) { item.Click += eventHandler; }

            return item;
        }

        public virtual void BuildContextMenu(ContextMenuStrip contextMenuStrip)
        {
            try
            {
                foreach (ToolStripItem item in TrayMenuStripCollection)
                {
                    contextMenuStrip.Items.Add(item);
                }
            }
            catch
            {
                
            }
        }

        private void SysTrayIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MethodInfo mi = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
                mi.Invoke(SysTrayIcon, null);
            }
        }

        private void ContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = false;
            SysTrayIcon.ContextMenuStrip.Items.Clear();
            BuildContextMenu(SysTrayIcon.ContextMenuStrip);
            SysTrayIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            SysTrayIcon.ContextMenuStrip.Items.Add(ToolStripMenuItemWithHandler("&Exit", exitItem_Click));
        }

        #endregion generic code framework


    }
}
