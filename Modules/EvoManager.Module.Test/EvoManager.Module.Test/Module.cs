﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvoManager.Module.Test
{
    public class Module
    {
        private Main mainForm;

        public void ShowMainForm()
        {
            if (mainForm == null)
            {
                mainForm = new Main();
                mainForm.Closed += mainForm_Closed; // avoid reshowing a disposed form
                mainForm.Show();
            }
            else { mainForm.Activate(); }
        }

        // null out the forms so we know to create a new one.
        private void mainForm_Closed(object sender, EventArgs e) { mainForm = null; }
    }
}
