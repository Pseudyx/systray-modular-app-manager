﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Text;

namespace EvoManager.Modules.Packager
{
    class Program
    {
        static void Main(string[] args)
        {
            CreatePackage();
        }

        private static void CreatePackage()
        {
            Console.WriteLine("Enter unPackaged Files location:");
            string rootPath = Console.ReadLine();
            
            #region Check Dir
            
            bool dirExists = Directory.Exists(rootPath);
            Console.WriteLine("\nDir Exists: " + dirExists.ToString());

            #endregion Check Dir
            
            if (dirExists)
            {
                bool abort = false;
                String ModuleFile = string.Empty;

                #region Check Primary Module File

                string[] ModuleFiles = Directory.GetFiles(rootPath, "EvoManager.Module.*.dll");
                if (ModuleFiles.Count() > 1)
                {
                    Console.WriteLine("\nMultiple Module files found:");
                    foreach (string file in ModuleFiles)
                    {
                        Console.WriteLine(file);
                    }
                    Console.WriteLine("\nPlease type the full path including name of the Primary Module file:");
                    ModuleFile = Console.ReadLine();
                    int cnt = 0;
                    while (!File.Exists(ModuleFile) && (cnt <= 2))
                    {
                        Console.WriteLine("\nFile does not exist. Try again.");
                        ModuleFile = Console.ReadLine();
                        cnt++;
                    }
                    if(!File.Exists(ModuleFile)){
                        Console.WriteLine("\nToo many failed attempts.");
                        abort = true; 
                    }
                }
                else if (ModuleFiles.Count() == 1)
                {
                    ModuleFile = ModuleFiles[0];
                }
                else { abort = true; }

                string[] ResourceFiles = Directory.GetFiles(rootPath).Where(x => x != ModuleFile).ToArray();

                #endregion Check Primary Module File

                if (!abort)
                {
                    #region Create Primary File Uri

                    Uri partUriDocument = PackUriHelper.CreatePartUri(
                                            new Uri(ModuleFile, UriKind.Relative));

                    #endregion Create Primary File Uri

                    #region Create Resource Files Uri
                    List<KeyValuePair<string,Uri>> ResourceList = new List<KeyValuePair<string,Uri>>();
                    foreach (string resource in ResourceFiles)
                    {
                        Uri partUriResource = PackUriHelper.CreatePartUri(
                            new Uri(resource, UriKind.Relative));
                        ResourceList.Add(new KeyValuePair<string,Uri>(resource,partUriResource));
                    }

                    #endregion Create Resource Files Uri

                    #region Create Package

                    bool PackageSuccess = false;
                    string packagePath = Path.Combine(rootPath, ModuleFile.Replace(".dll", ".mod"));
                    using (Package package = Package.Open(packagePath, FileMode.Create))
                    {
                        // Add the Document part to the Package
                        PackagePart packagePartDocument =
                            package.CreatePart(partUriDocument,
                                           System.Net.Mime.MediaTypeNames.Application.Octet,
                                           CompressionOption.NotCompressed);

                        // Copy the data to the Document Part 
                        using (FileStream fileStream = new FileStream(ModuleFile, FileMode.Open, FileAccess.Read))
                        {
                            CopyStream(fileStream, packagePartDocument.GetStream());
                        }
                        string PackageRelationshipType = "http://schemas.microsoft.com/package/2006/relationships/metadata/core-properties";
                        // Add a Package Relationship to the Document Part
                        package.CreateRelationship(packagePartDocument.Uri, TargetMode.Internal, PackageRelationshipType);

                        string ResourceRelationshipType = "http://schemas.microsoft.com/xps/2005/06/required-resource";

                        foreach (KeyValuePair<string,Uri> partUriResource in ResourceList)
                        {
                            // Add a Resource Part to the Package
                            PackagePart packagePartResource =
                                package.CreatePart(partUriResource.Value,
                                               System.Net.Mime.MediaTypeNames.Application.Octet);

                            // Copy the data to the Resource Part 
                            using (FileStream fileStream = new FileStream(partUriResource.Key, FileMode.Open, FileAccess.Read))
                            {
                                CopyStream(fileStream, packagePartResource.GetStream());
                            }

                            // Add Relationship from the Document part to the Resource part
                            packagePartDocument.CreateRelationship(
                                                    packagePartResource.Uri,
                                                    TargetMode.Internal,
                                                    ResourceRelationshipType);
                            
                        }

                        PackageSuccess = true;
                    }

                    #endregion Create Package

                    #region Clean up files
                    if (PackageSuccess)
                    {
                        Console.WriteLine("\nPackage Created Successfully: \n{0}", packagePath);
                        Console.WriteLine("\nClean up Module Files? (Y/N)");
                        String cleanResponse = Console.ReadKey(false).KeyChar.ToString();
                        while ((cleanResponse != "y" && cleanResponse != "n"))
                        {
                            Console.Clear();
                            Console.WriteLine("\nClean up Module Files? (y/n)");
                            cleanResponse = Console.ReadKey(false).KeyChar.ToString();
                        }

                        if (cleanResponse == "y")
                        {
                            try
                            {
                                foreach (string file in ResourceFiles)
                                {
                                    File.Delete(file);
                                }
                                File.Delete(ModuleFile);
                            }
                            catch { }
                        }
                      
                        Console.WriteLine("\n\nPackager Complete");
                    }

                    #endregion Clean up files
                }
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }


        //  --------------------------- CopyStream --------------------------- 
        /// <summary> 
        ///   Copies data from a source stream to a target stream.</summary> 
        /// <param name="source">
        ///   The source stream to copy from.</param> 
        /// <param name="target">
        ///   The destination stream to copy to.</param> 
        private static void CopyStream(Stream source, Stream target)
        {
            const int bufSize = 0x1000;
            byte[] buf = new byte[bufSize];
            int bytesRead = 0;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
                target.Write(buf, 0, bytesRead);
        }// end:CopyStream()
    }
}
