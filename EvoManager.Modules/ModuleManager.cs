﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;
using System.Security.Policy;

namespace EvoManager.Modules
{
    public class ModuleManager
    {
        public List<ModAssembly> ModLibrary;
        private bool useMod = true;

        public ModuleManager(string ModuleFolder)
        {
            try
            {
                ModLibrary = new List<ModAssembly>();

                string AppPath = AppDomain.CurrentDomain.BaseDirectory;
                string ModFolder = (string)Path.Combine(AppPath, ModuleFolder);
                var config = ModulesConfig.GetConfig();

                string usePackage = (ConfigurationManager.AppSettings["Package"] != null) ? ConfigurationManager.AppSettings["Package"] : "true";
                bool.TryParse(usePackage, out useMod);

                foreach (Module mod in config.Modules)
                {
                    if (useMod)
                    {

                        //Package Module
                        string modFile = (string)Path.Combine(ModFolder, mod.Assembly + ".mod");
                        if (File.Exists(modFile))
                        {
                            using (Package package = Package.Open(modFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                            {
                                string TempFolder = (string)Path.Combine(ModFolder, "." + mod.Name);
                                DirectoryInfo di = Directory.CreateDirectory(TempFolder);
                                di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;

                                #region Unpack Module

                                foreach (PackagePart part in package.GetParts())
                                {
                                    if (part.ContentType.ToLowerInvariant().StartsWith("application/"))
                                    {
                                        string target = Path.Combine(di.FullName, CreateFilenameFromUri(part.Uri));
                                        using (Stream source = part.GetStream(FileMode.Open, FileAccess.Read))
                                        using (Stream destination = File.OpenWrite(target))
                                        {
                                            byte[] buffer = new byte[0x1000];
                                            int read;
                                            while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                                            {
                                                destination.Write(buffer, 0, read);
                                            }
                                        }
                                    }
                                }

                                #endregion Unpack Module

                                #region Load Assemblies
                                string dllFile = (string)Path.Combine(TempFolder, mod.Assembly + ".dll");
                                if (File.Exists(dllFile))
                                {
                                    ModAssembly ModA = new ModAssembly();
                                    ModA.ModuleName = mod.Name;
                                    ModA.AssemblyName = mod.Assembly;
                                    ModA.Cache = TempFolder;

                                    FileStream fs = new FileStream(dllFile, FileMode.Open);
                                    byte[] buffer = new byte[(int)fs.Length];
                                    fs.Read(buffer, 0, buffer.Length);
                                    fs.Close();

                                    ModA.AssemblyInstance = Assembly.Load(buffer);
                                    //ModA.AssemblyInstance = Assembly.LoadFrom(dllFile);


                                    //Type[] types = ModA.AssemblyInstance.GetTypes();
                                    //foreach (Type t in types)
                                    //{
                                    //    MessageBox.Show(t.FullName);
                                    //}

                                    ModA.ModClass = ModA.AssemblyInstance.GetType(ModA.AssemblyName + ".Module");
                                    ModA.ModClassInstance = Activator.CreateInstance(ModA.ModClass);
                                    ModA.ModMethod = ModA.ModClass.GetMethod("ShowMainForm");

                                    ModLibrary.Add(ModA);
                                }
                                #endregion Load Assemblies
                            }
                        }
                    }
                    else
                    {
                        #region Load Assemblies from DLL 
                        
                        string modFile = (string)Path.Combine(ModFolder, mod.Assembly + ".dll");
                        if (File.Exists(modFile))
                        {
                            ModAssembly ModA = new ModAssembly();
                            ModA.ModuleName = mod.Name;
                            ModA.AssemblyName = mod.Assembly;
                            ModA.AssemblyInstance = Assembly.LoadFrom(modFile);
                            //Type[] types = ModA.AssemblyInstance.GetTypes();
                            //foreach (Type t in types)
                            //{
                            //    MessageBox.Show(t.FullName);
                            //}

                            ModA.ModClass = ModA.AssemblyInstance.GetType(ModA.AssemblyName + ".Module");
                            ModA.ModClassInstance = Activator.CreateInstance(ModA.ModClass);
                            ModA.ModMethod = ModA.ModClass.GetMethod("ShowMainForm");

                            ModLibrary.Add(ModA);
                        }

                        #endregion
                    }


                }
            }
            catch { }

        }

        public virtual void Dispose()
        {
            if (useMod)
            {
                foreach (ModAssembly mod in ModLibrary)
                {
                    DirectoryInfo di = new DirectoryInfo(mod.Cache);
                    if (di.Exists)
                    {
                        ////Create evidence for the new appdomain.
                        //Evidence adevidence = AppDomain.CurrentDomain.Evidence;

                        //// Create the new application domain.
                        //AppDomain domain = AppDomain.CreateDomain("MyDomain", adevidence);

                        //Console.WriteLine("Host domain: " + AppDomain.CurrentDomain.FriendlyName);
                        //Console.WriteLine("child domain: " + domain.FriendlyName);
                        //// Unload the application domain.
                        //AppDomain.Unload(AppDomain.CurrentDomain);

                        di.Delete(true);
                    }
                }
            }

            GC.Collect(); // collects all unused memory
            GC.WaitForPendingFinalizers(); // wait until GC has finished its work
            GC.Collect();
        }

        public void ModuleOpen_ToolStripMenuItemClickEvent(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            string name = item.Name;

            ModAssembly mod = ModLibrary.Where(x => x.ModuleName == name).FirstOrDefault();
            if (mod != null)
            {
                mod.ModMethod.Invoke(mod.ModClassInstance, null);
            }
        }

        public void ModuleOpen_DirectInvokeByName(string name)
        {
            ModAssembly mod = ModLibrary.Where(x => x.ModuleName == name).FirstOrDefault();
            if (mod != null)
            {
                mod.ModMethod.Invoke(mod.ModClassInstance, null);
            }
        }

        private string CreateFilenameFromUri(Uri uri)
        {
            char[] invalidChars = Path.GetInvalidFileNameChars();
            string stripDir = uri.OriginalString.Substring(uri.OriginalString.LastIndexOf('/')+1);
            StringBuilder sb = new StringBuilder(stripDir.Length);
            foreach (char c in stripDir)
            {
                sb.Append(Array.IndexOf(invalidChars, c) < 0 ? c : '_');
            }
            return sb.ToString();
        }
    }

    public class ModAssembly
    {
        private String moduleName;
        private Assembly assemblyInstance;
        private String assemblyName;
        private Type modClass;
        private MethodInfo modMethod;
        private Object obj;
        private String cache;

        public String ModuleName
        {
            get { return moduleName; }
            set { moduleName = value; }
        }

        public String AssemblyName
        {
            get { return assemblyName; }
            set { assemblyName = value; }
        }

        public Assembly AssemblyInstance
        {
            get { return assemblyInstance; }
            set { assemblyInstance = value; }
        }

        public Type ModClass
        {
            get { return modClass; }
            set { modClass = value; }
        }

        public Object ModClassInstance
        {
            get { return obj; }
            set { obj = value; }
        }

        public MethodInfo ModMethod
        {
            get { return modMethod; }
            set { modMethod = value; }
        }

        public String Cache
        {
            get { return cache; }
            set { cache = value; }
        }
    }
}
 