﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace EvoManager.Modules
{
    public class Module : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public String Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("assembly", IsRequired = true, IsKey = false)]
        public String Assembly
        {
            get
            {
                return this["assembly"] as string;
            }
        }
    }

    public class Modules : ConfigurationElementCollection
    {
        public Module this[int index]
        {
            get
            {
                return (Module)base.BaseGet(index);
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public new Module this[string responseString]
        {
            get { return (Module)BaseGet(responseString); }
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Module();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Module)element).Name;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "Module"; }
        }

    }

    public class ModulesConfig : ConfigurationSection
    {
        public static ModulesConfig GetConfig()
        {
            return (ModulesConfig)ConfigurationManager.GetSection("ModulesConfig") ?? new ModulesConfig();
        }

        [ConfigurationProperty("Modules")]
        [ConfigurationCollection(typeof(Modules), AddItemName = "Module")]
        public Modules Modules
        {
            get
            {
                return (Modules)base["Modules"];
            }
        }
    }
}
